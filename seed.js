var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./data/reviewsDB.sqlite');



//This setups the database tables and seeds the database with dummy data
//My Schema assumptions:
//A driver has many deliveries
//A delivery has many drivers but one review
//A delivery has one route
//A review  can only be left once the delivery is complete

db.serialize(function() {
	//Managers
	db.run("CREATE TABLE IF NOT EXISTS managers (managerId TEXT, name TEXT)");
	db.run("INSERT INTO managers (managerId, name) values(?,?)","MGR01","Manager 01");


	db.run("CREATE TABLE IF NOT EXISTS customers (customerId TEXT, name TEXT)");
	db.run("INSERT INTO customers (customerId, name) values(?,?)","U001","Andrew Landry");
	db.run("INSERT INTO customers (customerId, name) values(?,?)","U002","Jacob Poole");
	//Driver Table Schema
	db.run("CREATE TABLE IF NOT EXISTS drivers (driverId TEXT, managerId TEXT, name TEXT, currentLat double, currentLong double)");
	db.run("INSERT INTO drivers (driverId, managerId, name) values (?,?, ?)","AJL","MGR01","Andrew Landry");
	db.run("INSERT INTO drivers (driverId, managerId, name) values (?,?, ?)","DFL","MGR01","Donald Landry");


	//Delivery Table
	db.run("CREATE TABLE IF NOT EXISTS deliveries (deliveryId INTEGER, driverId TEXT, promiseTime date, completed BOOLEAN, PRIMARY KEY(deliveryId,driverId))");
	db.run("INSERT INTO deliveries (deliveryId, driverId, promiseTime, completed) values (?, ?,?, ?)",1,'AJL','2017-07-21 11:00:00',1);
	db.run("INSERT INTO deliveries (deliveryId, driverId, promiseTime, completed) values (?, ?,?, ?)",2,'DFL','2017-07-21 14:00:00',1);
	db.run("INSERT INTO deliveries (deliveryId, driverId, promiseTime, completed) values (?, ?,?, ?)",3,'AJL','2017-07-21 15:00:00',0);
	db.run("INSERT INTO deliveries (deliveryId, driverId, promiseTime, completed) values (?, ?,?, ?)",4,'DFL','2017-08-21 11:13:00',0);

	//delivery Route
	db.run("CREATE TABLE IF NOT EXISTS delivery_route (deliveryId INTEGER, pickupLat double, pickupLong double, dropOffLat double, dropOffLong double, PRIMARY KEY (deliveryId))")
	db.run("INSERT INTO delivery_route values (?,?,?,?,?)", 1, 30.200169, -92.086887, 30.219883, -92.043247);
	
	//Review Table
	db.run("CREATE TABLE IF NOT EXISTS reviews (deliveryId INTEGER PRIMARY KEY, rating INTEGER, description TEXT)");
	db.run("delete from reviews");
//	db.run("INSERT INTO reviews (deliveryId, rating, description) values (?, ?, ?)",1,5,'Didn\'t just put food by door like last guy :/ A++');
//	db.run("INSERT INTO reviews (deliveryId, rating, description) values (?, ?, ?)",2,1,'FOOD WAS COLD! WTF NVR DOING THIS SHIT AGN');
	

});
