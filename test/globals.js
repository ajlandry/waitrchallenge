//**** GLOBALS
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var chai = require('chai');
var chaiHttp = require('chai-http');
var sqlite3 = require('sqlite3').verbose();

chai.use(chaiHttp);

//***VARIABLES
var db =  new sqlite3.Database('test/TESTINGDB.sqlite3');
var port = 9000;
var server;


//Setup the Test database with dummy data
//TODO: convert the testing to use the express app object from the export of server.js main
//	Had to do this approach as I couldn't close the express app server cleanly which was locking the test database file for the deletion
//  on the after function of the test suite
before(function(done) {

	//database tables setup and seed
	db.serialize(function() {
		//Managers
		db.run("DROP TABLE IF EXISTS managers");
		db.run("CREATE TABLE IF NOT EXISTS managers (managerId TEXT, name TEXT)");
		db.run("INSERT INTO managers (managerId, name) values(?,?)","MGR01","Manager 01");


		db.run("DROP TABLE IF EXISTS customers");
		db.run("CREATE TABLE IF NOT EXISTS customers (customerId TEXT, name TEXT)");
		db.run("INSERT INTO customers (customerId, name) values(?,?)","U001","Andrew Landry");
		db.run("INSERT INTO customers (customerId, name) values(?,?)","U002","Jacob Poole");
		//Driver Table Schema

		db.run("DROP TABLE IF EXISTS drivers");
		db.run("CREATE TABLE IF NOT EXISTS drivers (driverId TEXT, managerId TEXT, name TEXT)");
		db.run("INSERT INTO drivers (driverId, managerId, name) values (?,?, ?)","TEST","MGR01","TESTING");

		//Delivery Table

		db.run("DROP TABLE IF EXISTS deliveries");
		db.run("CREATE TABLE IF NOT EXISTS deliveries (deliveryId INTEGER, driverId TEXT, promiseTime date, completed BOOLEAN, PRIMARY KEY(deliveryId,driverId))");
		db.run("INSERT INTO deliveries (deliveryId, driverId, promiseTime, completed) values (?, ?,?, ?)",1,'TEST','2017-07-21 11:00:00',1);
		db.run("INSERT INTO deliveries (deliveryId, driverId, promiseTime, completed) values (?, ?,?, ?)",2,'TEST','2017-07-21 14:00:00',1);
		
		//Review Table

		db.run("DROP TABLE IF EXISTS reviews");
		db.run("CREATE TABLE IF NOT EXISTS reviews (deliveryId INTEGER PRIMARY KEY, rating INTEGER, description TEXT)");
		db.run("INSERT INTO reviews (deliveryId, rating, description) values (?, ?, ?)",1,5,'Inital Review');
		

	});


	app.use(express.static('public'));
	app.get('/', function(req,res){
		res.sendFile('index.html');

	});
	app.get('/quit', function(req,res){
		res.send('closing...');
		server.close();
	})


	app.use(bodyParser.urlencoded({ extended: true }));
	require('../app/routes')(app, db);


	server = app.listen(port, () => {
		console.log('Test Server Starting on '+port);
	});

	done();
});



//close the server and database handle
//remove the test database file.
after(function(done){
	server.close();
	db.close(function(err){
		if(err){
			console.error(err.message);
		}
	});
	db = null;


	chai.request(server)
		.get('/quit')
		.end(function(err,res){
			console.log('Test Server Stopping....');

			var fs = require('fs');
			fs.unlinkSync('test/TESTINGDB.sqlite3');
			done();
		});


});


module.exports.port = port;
module.exports.serverHandle = app;