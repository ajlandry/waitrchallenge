var server = require('../test/globals.js').serverHandle
var chai = require('chai');
var chaiHttp = require('chai-http');

var should = chai.should();


chai.use(chaiHttp);


describe('Main Page', function (){
	it('should return html and have 200 status GET', function(done){
		chai.request(server)
			.get('/')
			.end(function(err, res){
				res.should.have.status(200);
				res.should.be.html;
				done();
			})
	})
});

describe('APIs', function() {


	//testing /api/drivers
	describe('Drivers', function() {
		
		it('should list ALL drivers on /api/drivers GET', function(done){
			chai.request(server)
				.get('/api/drivers/')
				.end(function(err, res){
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('array');
					done();
				});
		});

		it('should get test driver record on /api/drivers/:driver_Id GET', function(done){

			chai.request(server)
				.get('/api/drivers/TEST')
				.end(function(err, res){
					res.should.have.status(200);
					res.should.be.json;
					done();
				});
		});

		it('should return 404 when trying to post on /api/drivers POST', function(done){
			chai.request(server)
			.post('/api/drivers')
			.end(function(err,res){
				res.should.have.status(404);
				done();
			});
		});

	});

	describe('Deliveries', function() {
		it('should list ALL Deliveries on /api/deliveries GET', function(done){
			chai.request(server)
				.get('/api/deliveries')
				.end(function(err, res){
					res.should.have.status(200);
					res.should.be.json;
					done();
				});
		});

		it('should list Delivery for driverID on /api/deliveries/:driverId GET', function(done){
			chai.request(server)
			.get('/api/deliveries/TEST')
			.end(function(err, res){
				res.should.have.status(200);
				res.should.be.json;
				done();
			});
		});
	});

	describe('Reviews', function(){
		it('should list Reviews for a driverID on /api/drivers/:driverID/reviews GET', function(done){
			chai.request(server)
			.get('/api/drivers/TEST/reviews')
			.end(function(err, res){
				res.should.have.status(200);
				res.should.be.json;
				done();
			})
		})


		it('should get a review for a deliviery & driver on /api/drivers/:driverId/deliveries/:deliveryId/review GET', function(done){
			chai.request(server)
			.get('/api/drivers/TEST/deliveries/1/review')
			.end(function(err, res){
				res.should.have.status(200);
				done();
			});
		});

		it('should return 404 when trying to leave a second review on a delivery on /api/drivers/:driverId/deliveries/:deliveryId/review POST', function(done){
			chai.request(server)
			.post('/api/drivers/TEST/deliveries/1/review')
			.end(function(err, res){
				res.should.have.status(404);
				done();
			});
		});

		it('should successfully post a review for a completed delivery on /api/drivers/:driverId/deliveries/:deliveryId/review POST', function(done){
			chai.request(server)
			.post('/api/drivers/TEST/deliveries/2/review')
			.send({rating:4, description:'TEST Review'})
			.end(function(err, res){
				res.should.have.status(200);
				done();
			});
		});

	});

});


		