# Waitr Challenge - Driver Review App #

## Objective ##
Create a functional front/backend solution for posting a delivery review service. 
The backend is written using node.js and the data layer is a sql database using sqlite3 as the interface.
The frontend is a modified template using angular.js to communicate with the API for data retreival and posting.

## Reasoning ##
I went with a regular sql database as I'm extremely confortable with the SQL languange and was easier for me to verify data with the sqlite3 client.
I could always change this to a mongoDB if needed as I did install this on the server if needed.
The current implementation was written as the user is a driver manager and is reviewing reviews left by customers. My plan is to write a separate page
that would act as the client being able to leave a review on the delivery.

## Architectural ##
My data model consists of:

* A driver can have many deliveries.
* A delivery can have only one review and one assigned driver. In order to leave a review, the delivery must be completed.
* A review can only be assigned to one delivery. 

All API calls start at /api/{function}/....


## Resume ##
My resume can be found [here](https://www.dropbox.com/s/4psxfd0yg8ubsdu/Andrew%20Landry%20-%20Software%20Engineer%20-%20Resume.pdf?dl=0)

## Current Server ##
The current "production" server is running at [waitr.ajlandry.com](http://waitr.ajlandry.com) and is hosted at DigitalOcean.

## ToDos ##
[ ] Add location to model
[ ] Add Front-End piece for add new review
> Currently have the api written and working with POSTMAN

## Prerequisites ##
#### Server Side ###
* Ubuntu 16.04
* nginx/1.10.3
* NPM 3.10.10
* sqlite3 (used only if you wish to use the client command)


## Getting Started ##
I would recommend running the seeder to create the required tables and dummy data
~~~~
node seed.js
~~~~

#### Starting the Server ####

##### pm2 #####
pm2 is a process manager that allows the node apps to run headless and without an active console being used. 
[More info on pm2](http://pm2.io/)

Next to start the server using pm2
~~~~
npm run start:pm2
~~~~
This will register the application with the node process manager for you.

In order to shutdown using pm2
~~~~
npm run stop:pm2
~~~~

Also, you can restart
~~~~
npm run restart:pm2
~~~~

#### Testing ####

The testing suite is done with mocha and chai for the api requests.
The testing suite will: create a test database with seeded test data, test all the routes under app/routes and return the results.

To run test:
~~~
npm test
~~~




### Research, notes and errata ###
* I understood that Angular would be a quick way to implement the data retreval and sending but I wasn't sure on the template to use so I found one.
[Creating a single page todo app...](https://scotch.io/tutorials/creating-a-single-page-todo-app-with-node-and-angular)

* I'm considering migrating to mongodb with the mongoose client for easier schema definitions and route binding. And the main reason would be that the basis for node.js is for non-blocking data access.

* I discovered that sqlite3 driver will implicitly sanitize queries for you when running db.run with a query with params to bind.
* I have Nginx on a default config file that points to the default /var/www/html directory. The main change
was the port proxy to 8080 for location / that allows the user to interact with the node server.
~~~~
  location / {
        proxy_pass http://localhost:8080;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
~~~~