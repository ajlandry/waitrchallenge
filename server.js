//server.js

const express = require('express');
const bodyParser = require('body-parser');
const config = require('dotenv').config();
const app = express();
const resError = require('res-error');


const port = process.env.WEB_PORT;

var sqlite3 = require('sqlite3').verbose();
//using config/db to hold location of sqlite3 database
var SqlClient = new sqlite3.cached.Database(process.env.DB_LOCATION);

//Tells express we want to server the public folder as static content
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(resError);


app.get('/', function(req,res){
	res.sendFile('index.html');

});



require('./app/routes')(app, SqlClient);


app.listen(port, () => {
	console.log('We are live on '+port);
});
