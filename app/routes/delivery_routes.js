// routes/delivery_routes.js

module.exports = function(app,db) {

	app.get('/api/deliveries',(req,res) => {
		db.all("Select * from deliveries", function(err, rows) {
			res.json(rows);
		});

	});

	app.get('/api/deliveries/:driver_id',(req,res) => {
		db.all("select * from deliveries where driverId = ?", req.params.driver_id, (err,rows)=>{
		if (err) res.send(err);
		if (rows === null || rows === undefined)
		{
			res.status(404).send('Invalid Driver Id!');
		}
		else {
			res.json(rows);
		}
	});
	});

};
