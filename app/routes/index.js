// route/index.js


const driverRoutes = require('./driver_routes');
const deliveryRoutes = require('./delivery_routes');

module.exports = function(app,db) {
	driverRoutes(app,db);
	deliveryRoutes(app,db);

};
