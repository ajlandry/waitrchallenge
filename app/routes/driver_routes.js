// routes/driver_routes.js

module.exports = function(app,db) {

	//***************PARAM SETUP***************************
	//Setup request parameter driverID for our use (request.driverID)
	app.param('driver_id', function(req, res, next) {
		var dID = req.params.driver_id;
		req.driverID = dID.toUpperCase();
	//allow request to move to next function in stack
	next();
	});

	//Sets up the request parameter Delivery ID
	app.param('delivery_id', function(req, res,next) {
		req.deliveryID = req.params.delivery_id;
		next();
	});


	//***************ROUTE SETUP***************************
	app.get('/api/drivers',(req,res) => {
		db.all("Select * from drivers", function(err, rows) {
			res.json(rows);
		});

	});

	app.post('/api/drivers',(req,res) => {
		res.status(404).send("Not Allowed");
	});

	app.get('/api/drivers/:driver_id',(req, res) => {
		//query the database using the driver parameter
		//TODO: refactor for SQL injection; without a simple `OR 1=1` would attempt to fetch all drivers

		db.get("SELECT * from drivers where driverId= ?",req.driverID, function(err, row) {
			if (err){
				res.json(err);
			}
			if (row === undefined || row === null) {
				res.status(404).json('The driver does not exist.');
			} else {
			res.json(row);
			}
		});
	});

	app.get('/api/drivers/:driver_id/reviews',(req, res) => {
		//TODO: refactor for SQL injection; without a simple `OR 1=1` would attempt to fetch all drivers

		db.get("SELECT r.* from reviews r join deliveries d on r.deliveryId = d.deliveryId where d.driverId = ?",req.driverID, function(err, row) {
			if (err) res.send(err);
			if (row === undefined || row === null) {
				res.status(404).json('No reviews found...');
			} else {
			res.send(row);
			}
		});
	});
	app.get('/api/drivers/:driver_id/deliveries/:delivery_id/review',(req, res) => {
		
		db.get("SELECT r.* from reviews r join deliveries d on r.deliveryID = d.deliveryID where driverId = ? and d.deliveryId = ? and completed = 1",[req.driverID, req.deliveryID], function(err, row) {
			if (err) res.send(err);
			if (row === undefined || row === null) {
				res.status(404).send('Can\'t find this...');

			} else {
				res.send(row);
			}
		});

	});

	app.post('/api/drivers/:driver_id/deliveries/:delivery_id/review',(req, res) => {
		//query the database using the driver parameter
		//TODO: refactor for SQL injection; without a simple `OR 1=1` would attempt to fetch all drivers
		//1: Make sure driver exists
		//2: Ensure if delivery is assigned to the driver and that the delivery is markec Completed
		//3: allow data to post to database

		db.get("SELECT * from deliveries where driverId = ? and deliveryId = ? and completed = 1",[req.driverID, req.deliveryID], function(err, row) {

			if (err) res.send(err);
			if (row === undefined || row === null) {
					res.status(404).send('Specified Delivery is invalid or not complete! Please try again');
			} else {
				db.run("INSERT INTO reviews(deliveryID, rating, description) VALUES (?,?,?)",
					[req.deliveryID,req.body.rating,req.body.description],
					function (dbErr) {
						if (dbErr) {							
						 	res.status(404).send(dbErr);
						 }
						 else
						 {
							if(this.changes == 1)
							{
								console.log("WORK DONE - Informing API");	
								res.status(200).send('Review Saved!');
							} else {
								console.log("NOTHING DONE?");
								res.status(201).send('Review not saved! Try again');
							}
						}
					});

			}
		});



	});

};
