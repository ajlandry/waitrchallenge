// public/core.js
var scotchTodo = angular.module('scotchTodo', []);

function mainController($scope, $http) {
    $scope.formData = {};
    $scope.showReview = false;
    // when landing on the page, get all todos and show them
    $http.get('/api/drivers')
        .success(function(data) {
            $scope.drivers = data;
            
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

   //when user clicks on driver. show deliveries assigned to driver
	$scope.getDelivery = function(id) {
		$http.get('/api/deliveries/'+id)
		     .success(function(data) {
			if (Object.keys(data).length == 0)
			{
				data = null;
				var dumData = [{ deliveryId:0,promiseTime:' ---- No Deliveries! Get to work! ------' }];
				data = dumData;
			}
			$scope.deliveries = data;
			$scope.showReview= false;
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: '+ data);
		});
	

	};

	$scope.getReview = function(driverid, id) {
		$http.get('/api/drivers/'+driverid+'/deliveries/'+id+'/review')
			.success(function(data) {
				$scope.review = data;
				$scope.showReview = true;
			})
			.error(function(data) {
				$scope.showReview = true;
				var dummy = {rating: 0, description:'Not found!'};
				$scope.review = dummy;
				console.log('ERROR: '+ data);
			});

	};

    // when submitting the add form, send the text to the node API
    $scope.createTodo = function() {
        $http.post('/api/todos', $scope.formData)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.todos = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };



    // delete a todo after checking it
    $scope.deleteTodo = function(id) {
        $http.delete('/api/todos/' + id)
            .success(function(data) {
                $scope.todos = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

}

